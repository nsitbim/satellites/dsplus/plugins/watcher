async function watchPlugin(args, eventEmitter, [list, ds, alasql, debug]) {
  //debug("plugins:watch")(args)
  var code = await misc.login(args);
  if (code == 401) {
    misc.output("Authorization error", null, "red");
    process.exit(1);
  }
  args.assetsToList = ["job", "routine", "stage", "paramset"];
  var init = true;
  var existing = (await storage.keys())
    .filter((k) => k.match(/^watch:.*/))
    .map((k) => k.split(":")[1]);



  for (const e of existing) {
    alasql(`CREATE TABLE ${e}`);
    alasql(`SELECT * INTO ${e} FROM ?`, [await storage.getItem("watch:" + e)]);
  }

  while (true) {
    var projects = await list.projects(args);

    for (const p of projects.filter(p=>!(args.excludes||[]).includes(p.name))) {
      debug("plugins:watch")(p.name, "scanning for changes");
      args.project = p.name;
      args.update = true;
      args.metadata = true;
      args.git = true;
      if (!existing.includes(p.name)) {
        debug("plugins:watch")(args.project, "create state table");
        alasql(`CREATE TABLE ${args.project}`);
        existing.push(p.name);
      }
     
      await ds.init(JSON.parse(JSON.stringify(args)));

      storage.setItem(
        "watch:" + p.name,
        alasql(`SELECT * FROM ${args.project}`)
      );
    }

    await new Promise((resolve) => setTimeout(resolve, args.poll||3600));
  }
}

module.exports.watch = watchPlugin;
